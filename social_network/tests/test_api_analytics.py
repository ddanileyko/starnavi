from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from apps.users.models import User
from apps.posts.models import Post, Like

class APIUserTest(TestCase):

    fixtures = ['tests/test_data.json',]

    def setUp(self):
        pass

    def test_analytics_all(self):
        url = reverse('api:analytics-list')
        resp = self.client.get(url, {}, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), 3)
        self.assertEqual(resp.data[0]['added__date'], '2020-03-01')
        self.assertEqual(resp.data[0]['likes'], 3)
        self.assertEqual(resp.data[1]['added__date'], '2020-03-02')
        self.assertEqual(resp.data[1]['likes'], 3)
        self.assertEqual(resp.data[2]['added__date'], '2020-03-03')
        self.assertEqual(resp.data[2]['likes'], 3)

    def test_analytics_period(self):
        url = reverse('api:analytics-list')
        url = '%s%s' % (url, '?date_from=2020-03-02')
        resp = self.client.get(url, {}, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), 2)
        self.assertEqual(resp.data[0]['added__date'], '2020-03-02')
        self.assertEqual(resp.data[0]['likes'], 3)
        self.assertEqual(resp.data[1]['added__date'], '2020-03-03')
        self.assertEqual(resp.data[1]['likes'], 3)

        url = reverse('api:analytics-list')
        url = '%s%s' % (url, '?date_to=2020-03-02')
        resp = self.client.get(url, {}, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), 2)
        self.assertEqual(resp.data[0]['added__date'], '2020-03-01')
        self.assertEqual(resp.data[0]['likes'], 3)
        self.assertEqual(resp.data[1]['added__date'], '2020-03-02')
        self.assertEqual(resp.data[1]['likes'], 3)

        url = reverse('api:analytics-list')
        url = '%s%s' % (url, '?date_from=2020-03-02&date_to=2020-03-02')
        resp = self.client.get(url, {}, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), 1)
        self.assertEqual(resp.data[0]['added__date'], '2020-03-02')
        self.assertEqual(resp.data[0]['likes'], 3)
