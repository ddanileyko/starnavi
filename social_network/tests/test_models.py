from django.test import TestCase

from apps.users.models import User
from apps.posts.models import Post

class UserTest(TestCase):
    def setUp(self):
        User.objects.create(email='test-1@test.com', password='1234qwer')
        User.objects.create(email='test-2@test.com', password='1234qwer')

    def test_user(self):
        user_1 = User.objects.get(email='test-1@test.com')
        self.assertEqual(user_1.get_short_name(), 'test-1@test.com')
        user_2 = User.objects.get(email='test-2@test.com')
        self.assertEqual(user_2.get_short_name(), 'test-2@test.com')
        self.assertEqual(user_2.get_short_name(), 'test-2@test.com')

class PostTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(email='test-1@test.com', password='1234qwer')

    def test_transfer(self):
        post = Post(user=self.user, topic='12345', post='---12345---')
        self.assertEqual(post.__str__(), 'test-1@test.com -> 12345')

    def test_topic_label(self):
        post = Post(user=self.user, topic='12345', post='---12345---')
        field_label = post._meta.get_field('topic').verbose_name
        self.assertEquals(field_label, 'topic')

    def test_post_label(self):
        post = Post(user=self.user, topic='12345', post='---12345---')
        field_label = post._meta.get_field('post').verbose_name
        self.assertEquals(field_label, 'post')

    def test_topic_max_length(self):
        post = Post(user=self.user, topic='12345', post='---12345---')
        max_length = post._meta.get_field('topic').max_length
        self.assertEquals(max_length, 125)