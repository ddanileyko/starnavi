import jwt
from django.conf import settings

from apps.users.models import User, ActivityLog

class UserMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            token = request.headers.get('Authorization').split()[1]
            payload = jwt.decode(token, settings.SECRET_KEY)
            user_id = payload['user_id']
        except:
            response = self.get_response(request)
            return response
        user = User.objects.filter(pk=user_id).first()
        if user:
            log = ActivityLog(user=user)
            log.save()
        response = self.get_response(request)
        return response