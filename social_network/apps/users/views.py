import jwt
from rest_framework import status
from rest_framework.response import Response
from rest_framework_jwt.utils import jwt_payload_handler
from django.conf import settings
from django.contrib import auth

from .models import ActivityLog

from rest_framework_jwt.views import ObtainJSONWebToken


class LoginView(ObtainJSONWebToken):
    def post(self, request, *args, **kwargs):
        try:
            email = request.data['email']
            password = request.data['password']
            user = auth.authenticate(request, email=email, password=password)
            if user:
                try:
                    payload = jwt_payload_handler(user)
                    token = jwt.encode(payload, settings.SECRET_KEY)
                    user_details = {}
                    user_details['name'] = "%s" % (user.email)
                    user_details['token'] = token
                    log = ActivityLog(user=user, login=True)
                    log.save()
                    return Response(user_details, status=status.HTTP_200_OK)

                except Exception as e:
                    raise e
            else:
                res = {
                    'error': 'can not authenticate with the given credentials or the account has been deactivated'}
                return Response(res, status=status.HTTP_403_FORBIDDEN)
        except KeyError:
            res = {'error': 'please provide a email and a password'}
            return Response(res)
