from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django.db import models
from django.utils.translation import ugettext
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self, email, password, *args, **kwargs):
        if not email:
            msg = ugettext('User must have an email address')
            raise ValueError(msg)

        user = self.model(
            email=UserManager.normalize_email(email)
        )
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.email_checked = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email'), max_length=128, unique=True, db_index=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return '%s' % self.email

    def get_short_name(self):
        return self.email


class ActivityLog(models.Model):
    user = models.ForeignKey(User, related_name='from_user', on_delete=models.CASCADE)
    login = models.BooleanField(_('user login flag'), default=False)
    added = models.DateTimeField(_('added'), db_index=True, auto_now_add=True)

    class Meta:
        verbose_name = _('Activity')
        verbose_name_plural = _('Activity')

    def __str__(self):
        return u'%s -> %s' % (self.user.email, self.added)