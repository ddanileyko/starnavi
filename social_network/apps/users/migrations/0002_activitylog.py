# Generated by Django 2.2.11 on 2020-03-28 15:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActivityLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('login', models.BooleanField(default=False, verbose_name='user login flag')),
                ('added', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='added')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='from_user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Activity',
                'verbose_name': 'Activity',
            },
        ),
    ]
