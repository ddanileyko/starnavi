from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.users.models import User


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    topic = models.CharField(_('topic'), max_length=125)
    post = models.TextField(_('post'))
    added = models.DateTimeField(_('added'), db_index=True, auto_now_add=True)

    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Post')

    def __str__(self):
        return u'%s -> %s' % (self.user.email, self.topic)


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    added = models.DateTimeField(_('added'), db_index=True, auto_now_add=True)

    class Meta:
        verbose_name = _('Like')
        verbose_name_plural = _('Like')

    def __str__(self):
        return u'%s -> %s' % (self.user.email, self.post.topic)