from rest_framework import routers

from Api.User.views import UserAPIView, UserActivityView
from Api.Post.views import PostViewSet, AnalyticsViewSet


router_v1 = routers.SimpleRouter()

router_v1.register(r'user', UserAPIView, basename='user')
router_v1.register(r'posts', PostViewSet, basename='posts')
router_v1.register(r'activity', UserActivityView, basename='activity')
router_v1.register(r'analytics', AnalyticsViewSet, basename='analytics')
