from django.conf import settings
from rest_framework import serializers

from apps.users.models import User, ActivityLog


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )

    class Meta:
        model = User
        fields = ['email', 'password']

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class UserActivitySerializer(serializers.ModelSerializer):
    login_activity = serializers.SerializerMethodField()
    request_activity = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'email', 'login_activity', 'request_activity']

    def get_login_activity(self, obj):
        activity = ActivityLog.objects.filter(user=obj, login=True).order_by('-added').first()
        return activity.added if activity else None

    def get_request_activity(self, obj):
        activity = ActivityLog.objects.filter(user=obj, login=False).order_by('-added').first()
        return activity.added if activity else None