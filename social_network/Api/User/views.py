from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import api_view, action
from decimal import Decimal, getcontext

from Api.User.serializers import UserSerializer, UserActivitySerializer
from apps.users.models import User
from apps.posts.models import Post, Like


class UserActivityView(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserActivitySerializer


class UserAPIView(viewsets.ViewSet):
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        user = request.data
        serializer = UserSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(methods=['post'], detail=False, permission_classes=[AllowAny])
    def make_post(self, request, *args, **kwargs):
        if 'topic' in request.data and 'post' in request.data:
            post = Post(user=request.user, topic=request.data['topic'], post=request.data['post'])
            post.save()
            return Response({'post_id': post.id}, status=status.HTTP_201_CREATED)
        else:
            return Response('Request data error', status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=True, permission_classes=[AllowAny])
    def like(self, request, pk=None, *args, **kwargs):
        post = Post.objects.filter(pk=pk).first()
        if not post:
            return Response('Ivalid post id', status=status.HTTP_404_NOT_FOUND)
        like = Like.objects.filter(user=request.user, post=post)
        if like:
            return Response('Like already exists', status=status.HTTP_409_CONFLICT)
        else:
            like = Like(user=request.user, post=post)
            like.save()
            return Response('Ok', status=status.HTTP_201_CREATED)

    @action(methods=['post'], detail=True, permission_classes=[AllowAny])
    def unlike(self, request, pk=None, *args, **kwargs):
        post = Post.objects.filter(pk=pk).first()
        if not post:
            return Response('Ivalid post id', status=status.HTTP_404_NOT_FOUND)
        if post.user != request.user:
            return Response('Ivalid post id', status=status.HTTP_404_NOT_FOUND)
        like = Like.objects.filter(user=request.user, post=post)
        if not like:
            return Response('User do not liked post', status=status.HTTP_404_NOT_FOUND)
        else:
            like.delete()
            return Response('Ok', status=status.HTTP_200_OK)