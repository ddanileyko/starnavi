from rest_framework import serializers

from apps.posts.models import Post, Like


class PostSerializer(serializers.ModelSerializer):
    likes = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ['id', 'topic', 'post', 'user', 'likes']

    def get_likes(self, obj):
        return Like.objects.filter(post=obj).count()


class AnalyticsSerializer(serializers.Serializer):
    added__date = serializers.DateField(read_only=True)
    likes = serializers.IntegerField(read_only=True)