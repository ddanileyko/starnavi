from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import viewsets
from django.db.models import Count

from .serializers import PostSerializer, AnalyticsSerializer
from apps.posts.models import Post, Like


class PostViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Post.objects.all().order_by('-added')
    serializer_class = PostSerializer

    def get_queryset(self):
        qs = super(PostViewSet, self).get_queryset()
        qs = qs.order_by('-added')
        return qs


class AnalyticsViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Like.objects.values('added__date').annotate(likes=Count('id'))
    serializer_class = AnalyticsSerializer

    def get_queryset(self):
        qs = Like.objects.values('added__date').annotate(likes=Count('id'))
        date_from = self.request.GET.get('date_from', None)
        date_to = self.request.GET.get('date_to', None)
        if date_from:
            qs = qs.filter(added__date__gte=date_from)
        if date_to:
            qs = qs.filter(added__date__lte=date_to)
        return qs
