#!/usr/bin/env python
# -*- coing: UTF-8 -*- #

# import csv
# import operator
# from datetime import datetime, timedelta
import configparser
import requests
import json
from random import choice, randint
from string import ascii_letters

if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read('settings.ini')
    number_of_users = int(config['variables']['number_of_users'])
    max_post_per_user = int(config['variables']['max_post_per_user'])
    max_likes_per_user = int(config['variables']['max_likes_per_user'])

    r = requests.post(url='http://localhost:8000/api/login/',
                      json={"email": "admin@test.com", "password": "1234qwer"},
                      headers={"Content-Type": "application/json"})
    token = json.loads(r.text)['token']

    users = ['test-' + ''.join(choice(ascii_letters) for i in range(7)) + '-' + str(i)
             + '@test.com' for i in range(number_of_users)]
    for email in users:
        r = requests.post(url='http://localhost:8000/api/v1/user/',
                          json={"email": email, "password": "1234qwer"},
                          headers={"Content-Type": "application/json", "Authorization": "Bearer " + token})
    print('%s users created.' % str(len(users)))

    posts = []
    tokens = {}
    for email in users:
        r = requests.post(url='http://localhost:8000/api/login/',
                          json={"email": email, "password": "1234qwer"},
                          headers={"Content-Type": "application/json"})
        token = json.loads(r.text)['token']
        tokens[email] = token

        for i in range(randint(1, max_post_per_user)):
            post = ' '.join(''.join(choice(ascii_letters) for i in range(randint(3,10))) for j in range(randint(50, 100)))
            r = requests.post(url='http://localhost:8000/api/v1/user/make_post/',
                              json={"topic": ''.join(choice(ascii_letters) for i in range(7)),
                                    "post": post},
                              headers={"Content-Type": "application/json", "Authorization": "Bearer " + token})
            posts.append(json.loads(r.text)['post_id'])
    print('%s posts created.' % str(len(posts)))

    number_of_likes = 0
    for email in users:
        for i in range(randint(1, max_likes_per_user)):
            post = posts[randint(0, len(posts) - 1)]
            r = requests.post(url='http://localhost:8000/api/v1/user/' + str(post) + '/like/',
                              json={},
                              headers={"Content-Type": "application/json", "Authorization": "Bearer " + tokens[email]})
            if r.status_code == 201:
                number_of_likes += 1

    print('%s likes created.' % str(number_of_likes))

